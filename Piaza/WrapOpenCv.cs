﻿using System;
using System.IO;
using System.Threading;
using OpenCvSharp;

class WrapOpenCv
{
    // Token: 0x06000189 RID: 393 RVA: 0x00019538 File Offset: 0x00017738
    public static double ImageMatching(string srcImg, string dstImg, int nWidth, int nHeight, int nDensity, out Rect outRect)
    {
        for (int i = 0; i <= 4; i++)
        {
            try
            {
                return WrapOpenCv.ImageMatchingProc(srcImg, dstImg, nWidth, nHeight, nDensity, out outRect);
            }
            catch (Exception)
            {
                Thread.Sleep(1000);
            }
        }
        outRect = new Rect(0, 0, 0, 0);
        return 0.1;
    }

    // Token: 0x0600018A RID: 394 RVA: 0x0001959C File Offset: 0x0001779C
    public static double ImageMatchingProc(string srcImg, string dstImg, int nWidth, int nHeight, int nDensity, out Rect outRect)
    {
        double result;
        try
        {
            if (!File.Exists(srcImg))
            {
                outRect = new Rect(0, 0, 0, 0);
                result = 0.1;
            }
            else if (!File.Exists(dstImg))
            {
                outRect = new Rect(0, 0, 0, 0);
                result = 0.1;
            }
            else
            {
                Mat mat = new Mat(srcImg, ImreadModes.Unchanged);
                Mat mat2 = new Mat(dstImg, ImreadModes.Unchanged);
                Mat mat3 = new Mat();
                Mat mat4 = new Mat();
                Cv2.Resize(mat, mat3, new Size(mat.Width, mat.Height), 0.0, 0.0, InterpolationFlags.Linear);
                double num = (double)nWidth / 1080.0 * (double)mat2.Width;
                int num2 = (int)((double)nHeight / 1920.0 * (double)mat2.Height);
                int width = (int)num;
                int height = num2;
                if (mat.Width != nWidth)
                {
                    Cv2.Resize(mat2, mat4, new Size(width, height), 0.0, 0.0, InterpolationFlags.Linear);
                }
                else
                {
                    num = (double)nDensity / 480.0 * (double)mat2.Width;
                    int num3 = (int)((double)nDensity / 480.0 * (double)mat2.Height);
                    width = (int)num;
                    height = num3;
                    Cv2.Resize(mat2, mat4, new Size(width, height), 0.0, 0.0, InterpolationFlags.Linear);
                }
                Mat mat5 = mat3.CvtColor(ColorConversionCodes.BGR2RGB, 0);
                Mat mat6 = mat4.CvtColor(ColorConversionCodes.BGR2RGB, 0);
                Mat mat7 = new Mat(mat3.Rows - mat4.Rows + 1, mat3.Cols - mat4.Cols + 1, MatType.CV_32FC1);
                Cv2.MatchTemplate(mat5, mat6, mat7, TemplateMatchModes.CCoeffNormed, null);
                double num4;
                double num5;
                Point point;
                Point point2;
                Cv2.MinMaxLoc(mat7, out num4, out num5, out point, out point2, null);
                double num6 = num5;
                Rect rect = new Rect(new Point(point2.X, point2.Y), new Size(mat4.Width, mat4.Height));
                Cv2.Rectangle(mat3, rect, Scalar.Red, 2, LineTypes.Link8, 0);
                outRect = rect;
                mat.Release();
                mat2.Release();
                mat3.Release();
                mat4.Release();
                mat5.Release();
                mat6.Release();
                mat7.Release();
                result = num6;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("GetKeywordWork Fail\r\n" + "Src : " + srcImg + "\r\n" + "Dst : " + dstImg + "\r\n" + ex.ToString());
        }
        return result;
    }

    // Token: 0x0600018B RID: 395 RVA: 0x00019854 File Offset: 0x00017A54
    public static double ImageMatchingDebug(string srcImg, string dstImg, int nWidth, int nHeight, int nDensity, out Rect outRect)
    {
        double result;
        try
        {
            if (!File.Exists(srcImg))
            {
                outRect = new Rect(0, 0, 0, 0);
                result = 0.1;
            }
            else if (!File.Exists(dstImg))
            {
                outRect = new Rect(0, 0, 0, 0);
                result = 0.1;
            }
            else
            {
                Mat mat = new Mat(srcImg, ImreadModes.Unchanged);
                Mat mat2 = new Mat(dstImg, ImreadModes.Unchanged);
                Mat mat3 = new Mat();
                Mat mat4 = new Mat();
                Cv2.Resize(mat, mat3, new Size(mat.Width, mat.Height), 0.0, 0.0, InterpolationFlags.Linear);
                double num = (double)nWidth / 1080.0 * (double)mat2.Width;
                int num2 = (int)((double)nHeight / 1920.0 * (double)mat2.Height);
                int width = (int)num;
                int height = num2;
                if (1080 != nWidth)
                {
                    num = (double)nDensity / 480.0 * (double)mat2.Width;
                    int num3 = (int)((double)nDensity / 480.0 * (double)mat2.Height);
                    width = (int)num;
                    height = num3;
                    Cv2.Resize(mat2, mat4, new Size(width, height), 0.0, 0.0, InterpolationFlags.Linear);
                }
                else
                {
                    num = (double)nDensity / 480.0 * (double)mat2.Width;
                    int num4 = (int)((double)nDensity / 480.0 * (double)mat2.Height);
                    width = (int)num;
                    height = num4;
                    Cv2.Resize(mat2, mat4, new Size(width, height), 0.0, 0.0, InterpolationFlags.Linear);
                }
                Mat mat5 = mat3.CvtColor(ColorConversionCodes.BGR2GRAY, 0);
                Mat mat6 = mat4.CvtColor(ColorConversionCodes.BGR2GRAY, 0);
                Mat mat7 = new Mat(mat3.Rows - mat4.Rows + 1, mat3.Cols - mat4.Cols + 1, MatType.CV_32FC1);
                Cv2.MatchTemplate(mat5, mat6, mat7, TemplateMatchModes.CCoeffNormed, null);
                double num5;
                double num6;
                Point point;
                Point point2;
                Cv2.MinMaxLoc(mat7, out num5, out num6, out point, out point2, null);
                double num7 = num6;
                Rect rect = new Rect(new Point(point2.X, point2.Y), new Size(mat4.Width, mat4.Height));
                Cv2.Rectangle(mat3, rect, Scalar.Red, 2, LineTypes.Link8, 0);
                outRect = rect;
                Cv2.ImShow("dst", mat4);
                Cv2.ImShow("Matches", mat3);
                mat.Release();
                mat2.Release();
                mat3.Release();
                mat4.Release();
                mat5.Release();
                mat6.Release();
                mat7.Release();
                result = num7;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("GetKeywordWork Fail\r\n" + "Src : " + srcImg + "\r\n" + "Dst : " + dstImg + "\r\n" + ex.ToString());
        }
        return result;
    }

    // Token: 0x0600018C RID: 396 RVA: 0x00019B54 File Offset: 0x00017D54
    public static int IsSameImage(string srcImg, string dstImg)
    {
        int result = 0;
        Mat mat = new Mat(srcImg, ImreadModes.Unchanged);
        Mat mat2 = new Mat(dstImg, ImreadModes.Unchanged);
        Mat mat3 = mat.CvtColor(ColorConversionCodes.BGR2GRAY, 0);
        Mat mat4 = mat2.CvtColor(ColorConversionCodes.BGR2GRAY, 0);
        Mat mat5 = new Mat(mat.Rows - mat2.Rows + 1, mat.Cols - mat2.Cols + 1, MatType.CV_32FC1);
        Cv2.MatchTemplate(mat3, mat4, mat5, TemplateMatchModes.CCoeffNormed, null);
        double num;
        double num2;
        Point point;
        Point point2;
        Cv2.MinMaxLoc(mat5, out num, out num2, out point, out point2, null);
        if (num2 > 0.95)
        {
            result = 1;
        }
        mat.Release();
        mat2.Release();
        mat3.Release();
        mat4.Release();
        mat5.Release();
        return result;
    }
}

