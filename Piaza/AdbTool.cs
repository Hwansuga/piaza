﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Text;


class AdbTool
{
    // Token: 0x040000B7 RID: 183
    public string m_CurDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);

    // Token: 0x040000B8 RID: 184
    public string m_strSerial = "";

    // Token: 0x040000B9 RID: 185
    public int m_nWidth;

    // Token: 0x040000BA RID: 186
    public int m_nHeight;

    // Token: 0x040000BB RID: 187
    public int m_nDensity;

    // Token: 0x040000BC RID: 188
    public int m_nCaptureSecond;

    // Token: 0x040000BD RID: 189
    public Random rnd = new Random();

    public int RunAdbWait(string strArg, out string strReturn)
    {
        int result = 0;
        string fileName = m_CurDir + "\\adb.exe";
        ProcessStartInfo processStartInfo = new ProcessStartInfo();
        processStartInfo.FileName = fileName;
        processStartInfo.Arguments = strArg;
        processStartInfo.RedirectStandardOutput = true;
        processStartInfo.RedirectStandardError = true;
        processStartInfo.UseShellExecute = false;
        processStartInfo.CreateNoWindow = true;
        processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process process = new Process();
        process.StartInfo = processStartInfo;
        process.EnableRaisingEvents = true;
        try
        {
            process.Start();
            strReturn = process.StandardOutput.ReadToEnd();
        }
        catch (Exception)
        {
            strReturn = "";
            result = -1;
        }
        return result;
    }

    public  int RunAdbWaitWioutOutput(string strArg)
    {
        int result = 0;
        string fileName = m_CurDir + "\\adb.exe";
        ProcessStartInfo processStartInfo = new ProcessStartInfo();
        processStartInfo.FileName = fileName;
        processStartInfo.Arguments = strArg;
        processStartInfo.RedirectStandardOutput = true;
        processStartInfo.RedirectStandardInput = true;
        processStartInfo.RedirectStandardError = true;
        processStartInfo.UseShellExecute = false;
        processStartInfo.CreateNoWindow = true;
        Process process = new Process();
        process.StartInfo = processStartInfo;
        process.EnableRaisingEvents = false;
        try
        {
            process.Start();
            process.StandardOutput.ReadToEnd();
            process.StandardError.ReadToEnd();
            process.WaitForExit();
        }
        catch (Exception)
        {
            result = -1;
        }
        return result;
    }

    public  int RunAdb(string strArg)
    {
        int result = 0;
        string fileName = m_CurDir + "\\adb.exe";
        ProcessStartInfo processStartInfo = new ProcessStartInfo();
        processStartInfo.FileName = fileName;
        processStartInfo.Arguments = strArg;
        processStartInfo.RedirectStandardOutput = false;
        processStartInfo.RedirectStandardError = false;
        processStartInfo.UseShellExecute = false;
        processStartInfo.CreateNoWindow = true;
        processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
        Process process = new Process();
        process.StartInfo = processStartInfo;
        process.EnableRaisingEvents = true;
        try
        {
            process.Start();
        }
        catch (Exception)
        {
            result = -1;
        }
        return result;
    }

    public  int RunApk(string strCommand)
    {
        string strArg = "-s " + m_strSerial + " shell am start -n " + strCommand;
        return RunAdb(strArg);
    }

    public  int ForceStopApk(string strCommand)
    {
        string strArg = "-s " + m_strSerial + " shell am force-stop " + strCommand;
        return RunAdb(strArg);
    }

    // Token: 0x0600010D RID: 269 RVA: 0x00015368 File Offset: 0x00013568
    public  int DeleteDir(string strCommand)
    {
        string strArg = "-s " + m_strSerial + " shell rm -rf " + strCommand;
        return RunAdb(strArg);
    }

    // Token: 0x0600010E RID: 270 RVA: 0x00015394 File Offset: 0x00013594
    public  int GoWifiSetting()
    {
        string strArg = "-s " + m_strSerial + " shell am start -a android.intent.action.MAIN -n com.android.settings/.wifi.WifiSettings";
        return RunAdb(strArg);
    }

    // Token: 0x0600010F RID: 271 RVA: 0x000153C0 File Offset: 0x000135C0
    public  int GoHotSpotSetting()
    {
        string strArg = "-s " + m_strSerial + " shell am start -n com.android.settings/.TetherSettings";
        return RunAdb(strArg);
    }

    public  int GetWifiStatus()
    {
        string strArg = "-s " + m_strSerial + " shell \"dumpsys netstats | grep wlan\"";
        string s;
        RunAdbWait(strArg, out s);
        StringReader stringReader = new StringReader(s);
        ArrayList arrayList = new ArrayList();
        for (;;)
        {
            string text = stringReader.ReadLine();
            if (text == null)
            {
                break;
            }
            if (text.Length >= 5)
            {
                arrayList.Add(text);
            }
        }
        return arrayList.Count;
    }

    public int GetDataNetWorkStatus()
    {
        string strArg = "-s " + this.m_strSerial + " shell \"dumpsys telephony.registry | grep mDataConnectionLinkProperties\"";
        string s;
        this.RunAdbWait(strArg, out s);
        StringReader stringReader = new StringReader(s);
        int result = 0;
        string text;
        do
        {
            text = stringReader.ReadLine();
            if (text == null)
            {
                return result;
            }
        }
        while (text.Length < 5 || text.ToLower().IndexOf("linkaddresses") <= -1);
        result = 1;
        return result;
    }

    // Token: 0x06000112 RID: 274 RVA: 0x000154B4 File Offset: 0x000136B4
    public int GetHotspotStatus()
    {
        string strArg = "-s " + this.m_strSerial + " shell \"dumpsys wifi | grep mIsHotspotEnabled\"";
        string text;
        string text2;
        int result;
        if (this.GetAndroidVersion(out text) < 8)
        {
            strArg = "-s " + this.m_strSerial + " shell \"dumpsys wifi | grep curState=TetheredState\"";
            result = this.RunAdbWait(strArg, out text2);
            if (text2.Length < 10)
            {
                strArg = "-s " + this.m_strSerial + " shell \"dumpsys wifi | grep curState=ApOrStaEnabledState\"";
                result = this.RunAdbWait(strArg, out text2);
            }
            if (text2.Length < 10)
            {
                strArg = "-s " + this.m_strSerial + " shell \"dumpsys wifi | grep mIsHotspotEnabled\"";
                result = this.RunAdbWait(strArg, out text2);
            }
        }
        else
        {
            result = this.RunAdbWait(strArg, out text2);
        }
        StringReader stringReader = new StringReader(text2);
        result = 0;
        string text3;
        for (;;)
        {
            text3 = stringReader.ReadLine();
            if (text3 == null)
            {
                return result;
            }
            if (text3.Length >= 5 && text3.IndexOf("service_to_fd") <= -1)
            {
                if (text3.IndexOf("mIsHotspotEnabled") > -1 && text3.IndexOf("mIsHotspotEnabled") < 5)
                {
                    break;
                }
                if (text3.IndexOf("curState=TetheredState") > -1 && text3.IndexOf("curState=TetheredState") < 5)
                {
                    goto Block_11;
                }
                if (text3.IndexOf("curState=ApOrStaEnabledState") > -1 && text3.IndexOf("curState=ApOrStaEnabledState") < 5)
                {
                    goto Block_13;
                }
            }
        }
        if (text3.ToLower().IndexOf("true") > -1)
        {
            return 1;
        }
        return 0;
        Block_11:
        return 1;
        Block_13:
        result = 1;
        return result;
    }

    // Token: 0x06000113 RID: 275 RVA: 0x0001561C File Offset: 0x0001381C
    public int GetDeviceSerial(ref ObservableCollection<DeviceSerialElement> deviceList)
    {
        string s;
        int num = this.RunAdbWait("kill-server", out s);
        num = this.RunAdbWait("devices -l", out s);
        if (num != 0)
        {
            return num;
        }
        StringReader stringReader = new StringReader(s);
        ArrayList arrayList = new ArrayList();
        for (;;)
        {
            string text = stringReader.ReadLine();
            if (text == null)
            {
                break;
            }
            if (text.Length >= 1 && text.IndexOf("* daemon") <= -1 && text.IndexOf("List of") <= -1 && text.IndexOf("Emulator") <= -1 && text.IndexOf("emulator") <= -1)
            {
                arrayList.Add(text);
            }
        }
        if (deviceList == null)
        {
            deviceList = new ObservableCollection<DeviceSerialElement>();
        }
        deviceList.Clear();
        foreach (object obj in arrayList)
        {
            string[] array = ((string)obj).Split(new char[]
            {
                    ' '
            });
            string serial = array[0].Trim();
            string model = "";
            foreach (string text2 in array)
            {
                if (text2.IndexOf("model:") > -1)
                {
                    model = text2.Split(new char[]
                    {
                            ':'
                    })[1].Trim();
                    break;
                }
            }
            DeviceSerialElement deviceSerialElement = new DeviceSerialElement();
            deviceSerialElement.Serial = serial;
            deviceSerialElement.Model = model;
            deviceList.Add(deviceSerialElement);
        }
        return num;
    }

    // Token: 0x06000114 RID: 276 RVA: 0x000157A0 File Offset: 0x000139A0
    public int IsExistDeviceSerial(string strDeviceSerial)
    {
        string s;
        if (this.RunAdbWait("devices -l", out s) != 0)
        {
            return -1;
        }
        StringReader stringReader = new StringReader(s);
        ArrayList arrayList = new ArrayList();
        while (true)
        {
            string text = stringReader.ReadLine();
            if (text == null)
            {
                break;
            }
            if (text.Length >= 1 && text.IndexOf("List of") <= -1 && text.IndexOf("Emulator") <= -1 && text.IndexOf("emulator") <= -1)
            {
                arrayList.Add(text);
            }
        }

        IEnumerator enumerator = arrayList.GetEnumerator();
        {
            while (enumerator.MoveNext())
            {
                if (((string)enumerator.Current).Split(new char[]
                {
                        ' '
                })[0].Trim() == strDeviceSerial)
                {
                    return 1;
                }
            }
        }
        return 0;
    }

    // Token: 0x06000115 RID: 277 RVA: 0x00015880 File Offset: 0x00013A80
    public int KillServer()
    {
        string strArg = "kill-server";
        return this.RunAdb(strArg);
    }

    // Token: 0x06000116 RID: 278 RVA: 0x0001589C File Offset: 0x00013A9C
    public int StartServer()
    {
        string strArg = "start-server";
        return this.RunAdb(strArg);
    }

    // Token: 0x06000117 RID: 279 RVA: 0x000158B8 File Offset: 0x00013AB8
    public int DisplayOn()
    {
        string strArg = "-s " + this.m_strSerial + " shell input keyevent KEYCODE_WAKEUP ";
        return this.RunAdb(strArg);
    }

    // Token: 0x06000118 RID: 280 RVA: 0x000158E4 File Offset: 0x00013AE4
    public int DisplayOff()
    {
        string strArg = "-s " + this.m_strSerial + " shell input keyevent 223 ";
        return this.RunAdb(strArg);
    }

    // Token: 0x06000119 RID: 281 RVA: 0x00015910 File Offset: 0x00013B10
    public int IsDisplayOn()
    {
        string strArg = "-s " + this.m_strSerial + " shell dumpsys nfc | grep mScreenState";
        string s;
        if (this.RunAdbWait(strArg, out s) != 0)
        {
            return 0;
        }
        StringReader stringReader = new StringReader(s);
        string text = "";
        string[] array;
        for (;;)
        {
            string text2 = stringReader.ReadLine();
            if (text2 == null)
            {
                goto IL_7A;
            }
            if (text2.Length >= 5 && text2.IndexOf("mScreenState=") > -1)
            {
                array = text2.Split(new char[]
                {
                        '='
                });
                if (array.Length > 1)
                {
                    break;
                }
            }
        }
        text = array[1].Trim();
        IL_7A:
        text = text.ToUpper();
        if (text == "OFF")
        {
            return 0;
        }
        return 1;
    }

    // Token: 0x0600011A RID: 282 RVA: 0x000159B0 File Offset: 0x00013BB0
    public void Reboot()
    {
        string strArg = "-s " + this.m_strSerial + " reboot";
        this.RunAdb(strArg);
    }

    // Token: 0x0600011B RID: 283 RVA: 0x000159DC File Offset: 0x00013BDC
    public int PowerOnOff()
    {
        string strArg = "-s " + this.m_strSerial + " shell input keyevent KEYCODE_POWER ";
        return this.RunAdb(strArg);
    }

    // Token: 0x0600011C RID: 284 RVA: 0x00015A08 File Offset: 0x00013C08
    public int SetPhoneBatteryUnplug()
    {
        string strArg = "-s " + this.m_strSerial + " shell dumpsys battery unplug";
        return this.RunAdb(strArg);
    }

    // Token: 0x0600011D RID: 285 RVA: 0x00015A34 File Offset: 0x00013C34
    public int SetPhoneBatteryStatusDisCharging()
    {
        string strArg = "-s " + this.m_strSerial + " shell dumpsys battery set status 3";
        return this.RunAdb(strArg);
    }

    // Token: 0x0600011E RID: 286 RVA: 0x00015A60 File Offset: 0x00013C60
    public int SetPhoneBatteryReset()
    {
        string strArg = "-s " + this.m_strSerial + " shell dumpsys battery reset";
        return this.RunAdb(strArg);
    }

    // Token: 0x0600011F RID: 287 RVA: 0x00015A8C File Offset: 0x00013C8C
    public int SetPackageIdle(string strPname)
    {
        string strArg = string.Concat(new string[]
        {
                "-s ",
                this.m_strSerial,
                " shell am set-inactive ",
                strPname,
                " true"
        });
        return this.RunAdb(strArg);
    }

    // Token: 0x06000120 RID: 288 RVA: 0x00015AD4 File Offset: 0x00013CD4
    public string SetPhoneIdle()
    {
        string strArg = "-s " + this.m_strSerial + " shell dumpsys deviceidle step";
        string s;
        if (this.RunAdbWait(strArg, out s) != 0)
        {
            return "";
        }
        StringReader stringReader = new StringReader(s);
        new ArrayList();
        string result = "";
        for (;;)
        {
            string text = stringReader.ReadLine();
            if (text == null)
            {
                break;
            }
            if (text.Length >= 5)
            {
                string[] array = text.Split(new char[]
                {
                        ':'
                });
                if (array.Length > 1)
                {
                    result = array[1].Trim();
                }
            }
        }
        return result;
    }

    // Token: 0x06000121 RID: 289 RVA: 0x00015B5C File Offset: 0x00013D5C
    public void SetPhoneIdleState(string strPanme)
    {
        this.SetPhoneBatteryUnplug();
        this.DisplayOff();
        int num = 0;
        while (num <= 10 && !(this.SetPhoneIdle() == "IDLE"))
        {
            num++;
        }
        this.SetPackageIdle(strPanme);
    }

    // Token: 0x06000122 RID: 290 RVA: 0x00015BA0 File Offset: 0x00013DA0
    public void SetPhoneIdleState()
    {
        int num = 0;
        while (num <= 10 && !(this.SetPhoneIdle() == "IDLE"))
        {
            num++;
        }
    }

    // Token: 0x06000123 RID: 291 RVA: 0x00015BCC File Offset: 0x00013DCC
    public int InstallApk(string strApkPath)
    {
        string strArg = "-s " + this.m_strSerial + " install -r " + strApkPath;
        string text;
        return this.RunAdbWait(strArg, out text);
    }

    // Token: 0x06000124 RID: 292 RVA: 0x00015BFC File Offset: 0x00013DFC
    public int UnInstallApk(string strPackageName)
    {
        string strArg = "-s " + this.m_strSerial + " uninstall " + strPackageName;
        string text;
        return this.RunAdbWait(strArg, out text);
    }

    // Token: 0x06000125 RID: 293 RVA: 0x00015C2C File Offset: 0x00013E2C
    public int InstallCheck(string strPackageName)
    {
        string strArg = string.Concat(new string[]
        {
                "-s ",
                this.m_strSerial,
                " shell \"pm list packages | grep ",
                strPackageName,
                "\""
        });
        string s;
        int num = this.RunAdbWait(strArg, out s);
        if (num != 0)
        {
            return 0;
        }
        StringReader stringReader = new StringReader(s);
        new ArrayList();
        num = 0;
        string text;
        do
        {
            text = stringReader.ReadLine();
            if (text == null)
            {
                return num;
            }
        }
        while (text.Length < 1 || text.IndexOf(strPackageName) <= -1);
        num = 1;
        return num;
    }

    // Token: 0x06000126 RID: 294 RVA: 0x00015CB0 File Offset: 0x00013EB0
    public int SetIme()
    {
        string strArg = "-s " + this.m_strSerial + " shell ime set macaca.unicode.ime/.Utf7ImeService ";
        return this.RunAdb(strArg);
    }

    // Token: 0x06000127 RID: 295 RVA: 0x00015CDC File Offset: 0x00013EDC
    public int CaptureImage(string strDstPath)
    {
        DateTime now = DateTime.Now;
        int num = this.AdbCapture(strDstPath);
        if (num != 0)
        {
            num = this.AdbCapture(strDstPath);
        }
        if (num != 0)
        {
            num = this.AdbCapture(strDstPath);
        }
        this.m_nCaptureSecond = (DateTime.Now - now).Seconds;
        return num;
    }

    // Token: 0x06000128 RID: 296 RVA: 0x00015D28 File Offset: 0x00013F28
    public int AdbCapture(string strDstPath)
    {
        string strArg = "-s " + this.m_strSerial + " shell screencap -p /sdcard/screen.png ";
        string text;
        this.RunAdbWait(strArg, out text);
        string fileName = Path.GetFileName(strDstPath);
        string text2 = strDstPath.Replace(fileName, "down.png");
        strArg = "-s " + this.m_strSerial + " pull /sdcard/screen.png " + text2;
        this.RunAdbWait(strArg, out text);
        if (text.IndexOf("100%") < 0)
        {
            return -1;
        }
        while (!File.Exists(text2))
        {
        }
        File.Copy(text2, strDstPath, true);
        File.Delete(text2);
        strArg = "-s " + this.m_strSerial + " shell rm /sdcard/screen.png ";
        return this.RunAdbWait(strArg, out text);
    }

    // Token: 0x06000129 RID: 297 RVA: 0x00015DD0 File Offset: 0x00013FD0
    public int GetWmSize(out int Width, out int Height)
    {
        int result = 0;
        if (this.m_nWidth > 0 && this.m_nHeight > 0)
        {
            Width = this.m_nWidth;
            Height = this.m_nHeight;
            return result;
        }
        string s = "";
        string strArg = "-s " + this.m_strSerial + " shell wm size ";
        result = this.RunAdbWait(strArg, out s);
        StringReader stringReader = new StringReader(s);
        string text = "";
        string text2 = "";
        for (;;)
        {
            string text3 = stringReader.ReadLine();
            if (text3 == null)
            {
                break;
            }
            if (text3.Length >= 1)
            {
                if (text3.IndexOf("Physical") > -1)
                {
                    text = text3;
                }
                if (text3.IndexOf("Override") > -1)
                {
                    text2 = text3;
                }
            }
        }
        string text4;
        if (text2.Length > 1)
        {
            text4 = text2;
        }
        else
        {
            text4 = text;
        }
        string[] array = text4.Split(new char[]
        {
                ':'
        });
        int num = 0;
        int num2 = 0;
        if (array.Length > 1)
        {
            string[] array2 = array[1].Trim().Split(new char[]
            {
                    'x'
            });
            if (array2.Length > 1)
            {
                num = int.Parse(array2[0].Trim());
                num2 = int.Parse(array2[1].Trim());
            }
        }
        Width = num;
        Height = num2;
        this.m_nWidth = num;
        this.m_nHeight = num2;
        return result;
    }

    // Token: 0x0600012A RID: 298 RVA: 0x00015F14 File Offset: 0x00014114
    public int GetWmDensity(out int Density)
    {
        int result = 0;
        if (this.m_nDensity > 0)
        {
            Density = this.m_nDensity;
            return result;
        }
        string s = "";
        string strArg = "-s " + this.m_strSerial + " shell wm density ";
        result = this.RunAdbWait(strArg, out s);
        StringReader stringReader = new StringReader(s);
        string text = "";
        string text2 = "";
        for (;;)
        {
            string text3 = stringReader.ReadLine();
            if (text3 == null)
            {
                break;
            }
            if (text3.Length >= 1)
            {
                if (text3.IndexOf("Physical") > -1)
                {
                    text = text3;
                }
                if (text3.IndexOf("Override") > -1)
                {
                    text2 = text3;
                }
            }
        }
        string text4;
        if (text2.Length > 1)
        {
            text4 = text2;
        }
        else
        {
            text4 = text;
        }
        string[] array = text4.Split(new char[]
        {
                ':'
        });
        int num = 0;
        if (array.Length > 1)
        {
            num = int.Parse(array[1].Trim());
        }
        Density = num;
        this.m_nDensity = num;
        return result;
    }

    // Token: 0x0600012B RID: 299 RVA: 0x00016008 File Offset: 0x00014208
    public int ScrollUpShort()
    {
        int num = this.m_nWidth / 2;
        int num2 = this.m_nHeight / 2 - 100;
        int num3 = this.rnd.Next(num - 3, num + 3);
        int num4 = this.rnd.Next(num - 3, num + 3);
        int num5 = num2;
        int num6 = this.m_nHeight / 2 + 100;
        string strArg = string.Concat(new string[]
        {
                "-s ",
                this.m_strSerial,
                " shell input swipe ",
                num3.ToString(),
                " ",
                num5.ToString(),
                " ",
                num4.ToString(),
                " ",
                num6.ToString(),
                " 100"
        });
        string text = "";
        return this.RunAdbWait(strArg, out text);
    }

    // Token: 0x0600012C RID: 300 RVA: 0x000160E0 File Offset: 0x000142E0
    public int ScrollDownShort()
    {
        int num = this.m_nWidth / 2;
        int num2 = this.m_nHeight / 2 - 100;
        int num3 = this.rnd.Next(num - 3, num + 3);
        int num4 = this.rnd.Next(num - 3, num + 3);
        int num5 = this.m_nHeight / 2 + 80;
        int num6 = num2;
        string strArg = string.Concat(new string[]
        {
                "-s ",
                this.m_strSerial,
                " shell input swipe ",
                num3.ToString(),
                " ",
                num5.ToString(),
                " ",
                num4.ToString(),
                " ",
                num6.ToString(),
                " 100"
        });
        string text = "";
        return this.RunAdbWait(strArg, out text);
    }

    // Token: 0x0600012D RID: 301 RVA: 0x000161B8 File Offset: 0x000143B8
    public int ScrollDown()
    {
        int num = this.m_nWidth / 2;
        int num2 = this.m_nHeight / 2;
        int num3 = this.rnd.Next(num - 3, num + 3);
        int num4 = this.rnd.Next(num - 3, num + 3);
        int num5 = (int)((double)this.m_nHeight * 0.7);
        int num6 = (int)((double)this.m_nHeight * 0.2);
        string strArg = string.Concat(new string[]
        {
                "-s ",
                this.m_strSerial,
                " shell input swipe ",
                num3.ToString(),
                " ",
                num5.ToString(),
                " ",
                num4.ToString(),
                " ",
                num6.ToString(),
                " 700"
        });
        string text = "";
        return this.RunAdbWait(strArg, out text);
    }

    // Token: 0x0600012E RID: 302 RVA: 0x000162A4 File Offset: 0x000144A4
    public int ScrollUp()
    {
        int num = this.m_nWidth / 2;
        int num2 = this.m_nHeight / 2;
        int num3 = this.rnd.Next(num - 3, num + 3);
        int num4 = this.rnd.Next(num - 3, num + 3);
        int num5 = (int)((double)this.m_nHeight * 0.7);
        int num6 = (int)((double)this.m_nHeight * 0.2);
        string strArg = string.Concat(new string[]
        {
                "-s ",
                this.m_strSerial,
                " shell input swipe ",
                num3.ToString(),
                " ",
                num6.ToString(),
                " ",
                num4.ToString(),
                " ",
                num5.ToString(),
                " 700"
        });
        string text = "";
        return this.RunAdbWait(strArg, out text);
    }

    // Token: 0x0600012F RID: 303 RVA: 0x00016390 File Offset: 0x00014590
    public int ScrollRight()
    {
        int num = this.m_nWidth / 5;
        int num2 = this.m_nHeight / 2;
        int num3 = num * 4;
        int num4 = num;
        int num5 = num2;
        int num6 = num2;
        string strArg = string.Concat(new string[]
        {
                "-s ",
                this.m_strSerial,
                " shell input swipe ",
                num3.ToString(),
                " ",
                num5.ToString(),
                " ",
                num4.ToString(),
                " ",
                num6.ToString(),
                " 300"
        });
        string text = "";
        return this.RunAdbWait(strArg, out text);
    }

    // Token: 0x06000130 RID: 304 RVA: 0x0001643C File Offset: 0x0001463C
    public int EnterKeyCode(string strKeyCode)
    {
        string strArg = "-s " + this.m_strSerial + " shell input keyevent " + strKeyCode;
        string text = "";
        return this.RunAdbWait(strArg, out text);
    }

    // Token: 0x06000131 RID: 305 RVA: 0x00016470 File Offset: 0x00014670
    public int Tap(int X, int Y)
    {
        string strArg = string.Concat(new string[]
        {
                "-s ",
                this.m_strSerial,
                " shell input swipe ",
                X.ToString(),
                " ",
                Y.ToString(),
                " ",
                X.ToString(),
                " ",
                Y.ToString()
        });
        return this.RunAdbWaitWioutOutput(strArg);
    }

    // Token: 0x06000132 RID: 306 RVA: 0x000164EC File Offset: 0x000146EC
    public int DeleteInputBox()
    {
        string strArg = "-s " + this.m_strSerial + " shell input keyevent KEYCODE_MOVE_END ";
        this.RunAdbWaitWioutOutput(strArg);
        strArg = "-s " + this.m_strSerial + " shell am broadcast -a ADB_INPUT_CODE --ei code 67 --ei repeat 50";
        return this.RunAdbWaitWioutOutput(strArg);
    }

    // Token: 0x06000133 RID: 307 RVA: 0x00016534 File Offset: 0x00014734
    public int ChangeInputKeyboard()
    {
        string strArg = "-s " + this.m_strSerial + " shell ime set macaca.unicode.ime/.Utf7ImeService ";
        return this.RunAdb(strArg);
    }

    // Token: 0x06000134 RID: 308 RVA: 0x00016560 File Offset: 0x00014760
    public int TypeKeyWord(string strKeyWord, int nFirseDel)
    {
        string text = Convert.ToBase64String(Encoding.UTF8.GetBytes(strKeyWord));
        if (nFirseDel == 1)
        {
            this.DeleteInputBox();
        }
        string strArg = string.Concat(new string[]
        {
                "-s ",
                this.m_strSerial,
                " shell am broadcast -a ADB_INPUT_TEXT --es format base64 --es msg '",
                text,
                "'"
        });
        return this.RunAdbWaitWioutOutput(strArg);
    }

    // Token: 0x06000135 RID: 309 RVA: 0x000165C4 File Offset: 0x000147C4
    public int GoBack()
    {
        string strArg = "-s " + this.m_strSerial + " shell input keyevent KEYCODE_BACK ";
        return this.RunAdbWaitWioutOutput(strArg);
    }

    // Token: 0x06000136 RID: 310 RVA: 0x000165F0 File Offset: 0x000147F0
    public int GoSearch()
    {
        string strArg = "-s " + this.m_strSerial + " shell input keyevent KEYCODE_ENTER ";
        return this.RunAdbWaitWioutOutput(strArg);
    }

    // Token: 0x06000137 RID: 311 RVA: 0x0001661C File Offset: 0x0001481C
    public int GoAirPlaneSetting()
    {
        string strArg = "-s " + this.m_strSerial + " shell am start -a android.settings.AIRPLANE_MODE_SETTINGS ";
        return this.RunAdbWaitWioutOutput(strArg);
    }

    // Token: 0x06000138 RID: 312 RVA: 0x00016648 File Offset: 0x00014848
    public int GetAirPlaneMode()
    {
        string strArg = "-s " + this.m_strSerial + " shell settings get global airplane_mode_on ";
        string s = "";
        this.RunAdbWait(strArg, out s);
        StringReader stringReader = new StringReader(s);
        string s2 = "";
        for (;;)
        {
            string text = stringReader.ReadLine();
            if (text == null)
            {
                break;
            }
            if (text.Length >= 1)
            {
                s2 = text;
            }
        }
        return int.Parse(s2);
    }

    // Token: 0x06000139 RID: 313 RVA: 0x000166AC File Offset: 0x000148AC
    public int GetAndroidVersion(out string strVersion)
    {
        string strArg = "-s " + this.m_strSerial + " shell getprop ro.build.version.release ";
        string s = "";
        int result = this.RunAdbWait(strArg, out s);
        StringReader stringReader = new StringReader(s);
        string text = "";
        for (;;)
        {
            string text2 = stringReader.ReadLine();
            if (text2 == null)
            {
                break;
            }
            if (text2.Length >= 1)
            {
                text = text2;
            }
        }
        text = text.Replace(",", ".");
        strVersion = text;
        if (text.Length < 1)
        {
            result = -1;
        }
        else
        {
            result = int.Parse(text.Split(new char[]
            {
                    '.'
            })[0]);
        }
        return result;
    }

    // Token: 0x0600013A RID: 314 RVA: 0x0001674C File Offset: 0x0001494C
    public string GetCurrentFocus()
    {
        string strArg = "-s " + this.m_strSerial + " shell \"dumpsys window windows | grep -E mCurrentFocus\" ";
        string s = "";
        this.RunAdbWait(strArg, out s);
        StringReader stringReader = new StringReader(s);
        string result = "";
        for (;;)
        {
            string text = stringReader.ReadLine();
            if (text == null)
            {
                break;
            }
            if (text.Length >= 1)
            {
                result = text;
            }
        }
        return result;
    }

    // Token: 0x0600013B RID: 315 RVA: 0x000167A8 File Offset: 0x000149A8
    public int GetBatteryStatus()
    {
        string strArg = "-s " + this.m_strSerial + " shell dumpsys battery ";
        string s = "";
        int num = this.RunAdbWait(strArg, out s);
        if (num < 0)
        {
            return num;
        }
        StringReader stringReader = new StringReader(s);
        num = -1;
        string text2;
        for (;;)
        {
            string text = stringReader.ReadLine();
            if (text == null)
            {
                return num;
            }
            if (text.Length >= 1)
            {
                text2 = text;
                if (text2.IndexOf("level") > -1)
                {
                    break;
                }
            }
        }
        num = int.Parse(text2.Split(new char[]
        {
                ':'
        })[1].Trim());
        return num;
    }

    // Token: 0x0600013C RID: 316 RVA: 0x0001683C File Offset: 0x00014A3C
    public int AdbShellCommand(string command)
    {
        string strArg = "-s " + this.m_strSerial + " shell " + command;
        string text = "";
        return this.RunAdbWait(strArg, out text);
    }   
}

