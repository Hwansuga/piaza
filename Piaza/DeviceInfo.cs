﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class DeviceInfo
{
    public string Name { get; set; }
    public string DeviceID { get; set; }
    public string PNPDeviceID { get; set; }
    public string Description { get; set; }
    public string Caption { get; set; }
    public string Service { get; set; }
}

