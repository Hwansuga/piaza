﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using OpenCvSharp;

class ImageCompare : UserControl, IComponentConnector
{
    public ImageCompare()
    {
        this.m_CurDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
        this.InitializeComponent();
    }

    // Token: 0x06000159 RID: 345 RVA: 0x000169E0 File Offset: 0x00014BE0
    private void btnCompare_Click(object sender, RoutedEventArgs e)
    {
        string srcImg = this.m_CurDir + "\\Client01\\Capture\\capture.png";
        string dstImg = this.m_CurDir + "\\Images\\ImageMarkBrowser\\fontminus.png";
        OpenCvSharp.Rect rect;
        WrapOpenCv.ImageMatchingDebug(srcImg, dstImg, 1980, 1920, 640, out rect);
    }

    // Token: 0x0600015A RID: 346 RVA: 0x00016A28 File Offset: 0x00014C28
    [DebuggerNonUserCode]
    [GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    public void InitializeComponent()
    {
        if (this._contentLoaded)
        {
            return;
        }
        this._contentLoaded = true;
        Uri resourceLocator = new Uri("/Piaza;component/control/imagecompare.xaml", UriKind.Relative);
        Application.LoadComponent(this, resourceLocator);
    }

    // Token: 0x0600015B RID: 347 RVA: 0x00016A58 File Offset: 0x00014C58
    [DebuggerNonUserCode]
    [GeneratedCode("PresentationBuildTasks", "4.0.0.0")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    void IComponentConnector.Connect(int connectionId, object target)
    {
        if (connectionId == 1)
        {
            this.dockpanel1 = (DockPanel)target;
            return;
        }
        if (connectionId != 2)
        {
            this._contentLoaded = true;
            return;
        }
        this.btnCompare = (Button)target;
        this.btnCompare.Click += this.btnCompare_Click;
    }

    // Token: 0x040000CA RID: 202
    public string m_CurDir = "";

    // Token: 0x040000CB RID: 203
    internal DockPanel dockpanel1;

    // Token: 0x040000CC RID: 204
    internal Button btnCompare;

    // Token: 0x040000CD RID: 205
    private bool _contentLoaded;
}

