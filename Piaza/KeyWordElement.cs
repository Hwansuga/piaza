﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class KeyWordElement
{
    public string MainKeyWord { get; set; }

    public string SubKeyWord { get; set; }

    public string JobName { get; set; }

    public int JobType { get; set; }

    public string RowNum { get; set; }

    public int SetCount { get; set; }

    public int CurCount { get; set; }

    public int CurCycle { get; set; }

    public string ExtraInfo { get; set; }

    public string GroupName { get; set; }

    public int DupMain { get; set; }

    public int DupSub { get; set; }

    public int Empty { get; set; }
}

