﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

class DeviceSerialElement
{
    public string Serial { get; set; }

    // Token: 0x17000019 RID: 25
    // (get) Token: 0x06000140 RID: 320 RVA: 0x000168B8 File Offset: 0x00014AB8
    // (set) Token: 0x06000141 RID: 321 RVA: 0x000168C0 File Offset: 0x00014AC0
    public string Model { get; set; }

    // Token: 0x1700001A RID: 26
    // (get) Token: 0x06000142 RID: 322 RVA: 0x000168C9 File Offset: 0x00014AC9
    // (set) Token: 0x06000143 RID: 323 RVA: 0x000168D1 File Offset: 0x00014AD1
    public BitmapImage Thumbnail { get; set; }

    // Token: 0x1700001B RID: 27
    // (get) Token: 0x06000144 RID: 324 RVA: 0x000168DA File Offset: 0x00014ADA
    // (set) Token: 0x06000145 RID: 325 RVA: 0x000168E2 File Offset: 0x00014AE2
    public string CaptureTime { get; set; }

    // Token: 0x1700001C RID: 28
    // (get) Token: 0x06000146 RID: 326 RVA: 0x000168EB File Offset: 0x00014AEB
    // (set) Token: 0x06000147 RID: 327 RVA: 0x000168F3 File Offset: 0x00014AF3
    public DateTime LastAct { get; set; }

    // Token: 0x1700001D RID: 29
    // (get) Token: 0x06000148 RID: 328 RVA: 0x000168FC File Offset: 0x00014AFC
    // (set) Token: 0x06000149 RID: 329 RVA: 0x00016904 File Offset: 0x00014B04
    public string LastError { get; set; }

    // Token: 0x1700001E RID: 30
    // (get) Token: 0x0600014A RID: 330 RVA: 0x0001690D File Offset: 0x00014B0D
    // (set) Token: 0x0600014B RID: 331 RVA: 0x00016915 File Offset: 0x00014B15
    public int ReportError { get; set; }

    // Token: 0x1700001F RID: 31
    // (get) Token: 0x0600014C RID: 332 RVA: 0x0001691E File Offset: 0x00014B1E
    // (set) Token: 0x0600014D RID: 333 RVA: 0x00016926 File Offset: 0x00014B26
    public string IdFilePath { get; set; }

    // Token: 0x17000020 RID: 32
    // (get) Token: 0x0600014E RID: 334 RVA: 0x0001692F File Offset: 0x00014B2F
    // (set) Token: 0x0600014F RID: 335 RVA: 0x00016937 File Offset: 0x00014B37
    public string IdFileName { get; set; }

    // Token: 0x17000021 RID: 33
    // (get) Token: 0x06000150 RID: 336 RVA: 0x00016940 File Offset: 0x00014B40
    // (set) Token: 0x06000151 RID: 337 RVA: 0x00016948 File Offset: 0x00014B48
    public int NetWorkType { get; set; }

    // Token: 0x17000022 RID: 34
    // (get) Token: 0x06000152 RID: 338 RVA: 0x00016951 File Offset: 0x00014B51
    // (set) Token: 0x06000153 RID: 339 RVA: 0x00016959 File Offset: 0x00014B59
    public int ExistBrowserLogOut { get; set; }

    // Token: 0x17000023 RID: 35
    // (get) Token: 0x06000154 RID: 340 RVA: 0x00016962 File Offset: 0x00014B62
    // (set) Token: 0x06000155 RID: 341 RVA: 0x0001696A File Offset: 0x00014B6A
    public int ClientNum { get; set; }

    // Token: 0x06000156 RID: 342 RVA: 0x00016974 File Offset: 0x00014B74
    public void SetImage(string strPath)
    {
        BitmapImage bitmapImage = new BitmapImage();
        bitmapImage.BeginInit();
        bitmapImage.UriSource = new Uri(strPath);
        bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
        bitmapImage.EndInit();
        this.Thumbnail = bitmapImage;
    }
}

