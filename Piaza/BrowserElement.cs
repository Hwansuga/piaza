﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class BrowserElement
{
    public int Index { get; set; }
    public DateTime LastAct { get; set; }
    public DateTime StartDate { get; set; }
    public int nReboot { get; set; }
    public int nLogin { get; set; }
}

