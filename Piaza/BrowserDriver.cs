﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;

class BrowserDriver
{
    public string GetCapturePath()
    {
        string text = this.m_CurDir + "\\Client" + string.Format("{0:D2}", this.m_nClientNum) + "\\Capture";
        if (!Directory.Exists(text))
        {
            Directory.CreateDirectory(text);
        }
        return text + "\\capture.png";
    }

    // Token: 0x060000A9 RID: 169 RVA: 0x00011C84 File Offset: 0x0000FE84
    public string GetCapturePathSave()
    {
        string text = this.m_CurDir + "\\Client" + string.Format("{0:D2}", this.m_nClientNum) + "\\Capture";
        if (!Directory.Exists(text))
        {
            Directory.CreateDirectory(text);
        }
        return text + "\\captureSave.png";
    }

    // Token: 0x060000AA RID: 170 RVA: 0x00011CD6 File Offset: 0x0000FED6
    public string GetImagePath(string strImageName)
    {
        return this.m_CurDir + "\\Images\\ImageMarkBrowser\\" + strImageName;
    }

    // Token: 0x060000AB RID: 171 RVA: 0x00011CEC File Offset: 0x0000FEEC
    public void SetSerial(string strSerial)
    {
        this.m_strSerial = strSerial;
        this.adb.m_strSerial = strSerial;
        if (this.m_Width == 0 || this.m_Height == 0 || this.m_nDensity == 0)
        {
            this.adb.GetWmSize(out this.m_Width, out this.m_Height);
            this.adb.GetWmDensity(out this.m_nDensity);
            this.adb.ChangeInputKeyboard();
        }
    }

    // Token: 0x060000AC RID: 172 RVA: 0x00011D5A File Offset: 0x0000FF5A
    public void StartBrowser(int nBrowser)
    {
        this.adb.DisplayOn();
        this.m_nClientNum = nBrowser;
        this.m_strNNB = "";
        new Thread(new ThreadStart(this.StartBrowserWork))
        {
            IsBackground = true
        }.Start();
    }

    // Token: 0x060000AD RID: 173 RVA: 0x00011D98 File Offset: 0x0000FF98
    public void StartBrowserWork()
    {
        ChromeOptions chromeOptions = new ChromeOptions();
        //DesiredCapabilities desiredCapabilities = DesiredCapabilities.Chrome();
        string argument = string.Format("--user-data-dir=/mnt/sdcard/zRun/{0:D3}", this.m_nClientNum);
        chromeOptions.AddArgument(argument);
        chromeOptions.AddAdditionalCapability("androidPackage", this.m_strChromiumPackage);
        chromeOptions.AddAdditionalCapability("androidDeviceSerial", this.m_strSerial);
//         desiredCapabilities.SetCapability("device", "Android");
//         desiredCapabilities.SetCapability("platformName", "Android");
//         desiredCapabilities.SetCapability("platformVersion", "4.4.2");
//         desiredCapabilities.SetCapability("deviceName", "Android Emulator");
//         desiredCapabilities.SetCapability("browserName", "Chrome");
        string driverPath = this.m_CurDir + "\\vm\\" + this.m_nClientNum;
        this.service = ChromeDriverService.CreateDefaultService(driverPath);
        this.service.HideCommandPromptWindow = true;
        this.driver = new ChromeDriver(this.service, chromeOptions);
        this.SetPageLoadTimeOut();
        this.driver.Navigate().GoToUrl("https://m.naver.com");
        this.SetPageLoadTimeOut();
        this.m_strNNB = this.GetNNB();
    }

    // Token: 0x060000AE RID: 174 RVA: 0x00011EB4 File Offset: 0x000100B4
    public string GetNNB()
    {
        string text = "";
        string text2 = "";
        if (this.m_strNNB.Length > 2)
        {
            return this.m_strNNB;
        }
        try
        {
            text2 = (string)(this.driver as IJavaScriptExecutor).ExecuteScript("return document.cookie;", new object[0]);
        }
        catch (Exception)
        {
            return this.m_strNNB;
        }
        int num = text2.IndexOf("NNB=");
        if (num >= 0)
        {
            int num2 = text2.IndexOf(";", num + 4);
            string text3;
            if (num2 > -1)
            {
                text3 = text2.Substring(num, num2 - num + 1);
            }
            else
            {
                text3 = text2.Substring(num);
            }
            text = text3.Split(new char[]
            {
                    '='
            })[1];
        }
        this.m_strNNB = text;
        return text;
    }

    // Token: 0x060000AF RID: 175 RVA: 0x00011F88 File Offset: 0x00010188
    public void SetPageLoadTimeOut()
    {
        try
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(500.0);

            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(500.0);
        }
        catch (Exception)
        {
            throw;
        }
    }

    // Token: 0x060000B0 RID: 176 RVA: 0x00011FF4 File Offset: 0x000101F4
    public void SetPageLoadTimeOutWithoutThrow()
    {
        try
        {
            this.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10.0);
            this.driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(10.0);
        }
        catch (Exception)
        {
            throw;
        }
    }

    // Token: 0x060000B1 RID: 177 RVA: 0x00012060 File Offset: 0x00010260
    public bool IsElementPresentUnderElement(IWebElement mother, By by)
    {
        this.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0.0);
        bool result;
        try
        {
            mother.FindElement(by);
            result = true;
        }
        catch (NoSuchElementException)
        {
            result = false;
        }
        finally
        {
            this.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20.0);
        }
        return result;
    }

    // Token: 0x060000B2 RID: 178 RVA: 0x000120E4 File Offset: 0x000102E4
    public bool IsElementPresent(By by)
    {
        this.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0.0);
        bool result;
        try
        {
            if (this.driver.FindElement(by).Displayed)
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
        catch (Exception)
        {
            result = false;
        }
        return result;
    }

    // Token: 0x060000B3 RID: 179 RVA: 0x00012148 File Offset: 0x00010348
    public bool IsElementsPresent(By by)
    {
        this.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0.0);
        bool result;
        try
        {
            if (this.driver.FindElements(by)[0].Displayed)
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
        catch (NoSuchElementException)
        {
            result = false;
        }
        finally
        {
            this.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20.0);
        }
        return result;
    }

    // Token: 0x060000B4 RID: 180 RVA: 0x000121E4 File Offset: 0x000103E4
    public void ClickElement(IWebElement ele)
    {
        int num = this.rnd.Next(0, 1);
        int num2 = 0;
        int num3 = 0;
        if (num == 1)
        {
            num2 *= -1;
            num3 *= -1;
        }
        try
        {
            int x = ele.Location.X;
            int y = ele.Location.Y;
            int width = ele.Size.Width;
            int height = ele.Size.Height;
            int num4 = this.rnd.Next(0, 1);
            num2 = this.rnd.Next(0, width / 2);
            num3 = this.rnd.Next(0, height / 2);
            if (num4 == 1)
            {
                num2 *= -1;
                num3 *= -1;
            }
            new Actions(this.driver).MoveToElement(ele).MoveByOffset(num2, num3).Click().Build().Perform();
            this.SetPageLoadTimeOut();
        }
        catch (UnhandledAlertException)
        {
            try
            {
                IAlert alert = this.driver.SwitchTo().Alert();
                string text = alert.Text;
                alert.Accept();
                this.driver.SwitchTo().Window(this.driver.WindowHandles.First<string>());
                int x2 = ele.Location.X;
                int y2 = ele.Location.Y;
                int width = ele.Size.Width;
                int height = ele.Size.Height;
                int num5 = this.rnd.Next(0, 1);
                num2 = this.rnd.Next(0, width / 2);
                num3 = this.rnd.Next(0, height / 2);
                if (num5 == 1)
                {
                    num2 *= -1;
                    num3 *= -1;
                }
                new Actions(this.driver).MoveToElement(ele).MoveByOffset(num2, num3).Click().Build().Perform();
                this.SetPageLoadTimeOut();
            }
            catch (UnhandledAlertException)
            {
                try
                {
                    IAlert alert2 = this.driver.SwitchTo().Alert();
                    string text2 = alert2.Text;
                    alert2.Accept();
                    this.driver.SwitchTo().Window(this.driver.WindowHandles.First<string>());
                    int x3 = ele.Location.X;
                    int y3 = ele.Location.Y;
                    int width = ele.Size.Width;
                    int height = ele.Size.Height;
                    int num6 = this.rnd.Next(0, 1);
                    num2 = this.rnd.Next(0, width / 2);
                    num3 = this.rnd.Next(0, height / 2);
                    if (num6 == 1)
                    {
                        num2 *= -1;
                        num3 *= -1;
                    }
                    new Actions(this.driver).MoveToElement(ele).MoveByOffset(num2, num3).Click().Build().Perform();
                    this.SetPageLoadTimeOut();
                }
                catch (UnhandledAlertException)
                {
                }
            }
        }
    }

    // Token: 0x060000B5 RID: 181 RVA: 0x000124DC File Offset: 0x000106DC
    public void ClickElementNoWait(IWebElement ele)
    {
        int num = this.rnd.Next(0, 1);
        int num2 = 0;
        int num3 = 0;
        if (num == 1)
        {
            num2 *= -1;
            num3 *= -1;
        }
        try
        {
            int x = ele.Location.X;
            int y = ele.Location.Y;
            int width = ele.Size.Width;
            int height = ele.Size.Height;
            int num4 = this.rnd.Next(0, 1);
            num2 = this.rnd.Next(0, width / 2);
            num3 = this.rnd.Next(0, height / 2);
            if (num4 == 1)
            {
                num2 *= -1;
                num3 *= -1;
            }
            new Actions(this.driver).MoveToElement(ele).MoveByOffset(num2, num3).Click().Build().Perform();
        }
        catch (UnhandledAlertException)
        {
            try
            {
                IAlert alert = this.driver.SwitchTo().Alert();
                string text = alert.Text;
                alert.Accept();
                this.driver.SwitchTo().Window(this.driver.WindowHandles.First<string>());
                int x2 = ele.Location.X;
                int y2 = ele.Location.Y;
                int width = ele.Size.Width;
                int height = ele.Size.Height;
                int num5 = this.rnd.Next(0, 1);
                num2 = this.rnd.Next(0, width / 2);
                num3 = this.rnd.Next(0, height / 2);
                if (num5 == 1)
                {
                    num2 *= -1;
                    num3 *= -1;
                }
                new Actions(this.driver).MoveToElement(ele).MoveByOffset(num2, num3).Click().Build().Perform();
            }
            catch (UnhandledAlertException)
            {
                try
                {
                    IAlert alert2 = this.driver.SwitchTo().Alert();
                    string text2 = alert2.Text;
                    alert2.Accept();
                    this.driver.SwitchTo().Window(this.driver.WindowHandles.First<string>());
                    int x3 = ele.Location.X;
                    int y3 = ele.Location.Y;
                    int width = ele.Size.Width;
                    int height = ele.Size.Height;
                    int num6 = this.rnd.Next(0, 1);
                    num2 = this.rnd.Next(0, width / 2);
                    num3 = this.rnd.Next(0, height / 2);
                    if (num6 == 1)
                    {
                        num2 *= -1;
                        num3 *= -1;
                    }
                    new Actions(this.driver).MoveToElement(ele).MoveByOffset(num2, num3).Click().Build().Perform();
                }
                catch (UnhandledAlertException)
                {
                }
            }
        }
    }

    // Token: 0x04000074 RID: 116
    public AdbTool adb = new AdbTool();

    // Token: 0x04000075 RID: 117
    public string m_strSerial = "";

    // Token: 0x04000076 RID: 118
    public string m_CurDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);

    // Token: 0x04000077 RID: 119
    public int m_Width;

    // Token: 0x04000078 RID: 120
    public int m_Height;

    // Token: 0x04000079 RID: 121
    public int m_nDensity;

    // Token: 0x0400007A RID: 122
    public Random rnd = new Random();

    // Token: 0x0400007B RID: 123
    public int m_nStop;

    // Token: 0x0400007C RID: 124
    public string m_strChromiumPackage = "org.chromium.chrome.run01";

    // Token: 0x0400007D RID: 125
    public int m_nClientNum;

    // Token: 0x0400007E RID: 126
    public IWebDriver driver;

    // Token: 0x0400007F RID: 127
    public ChromeDriverService service;

    // Token: 0x04000080 RID: 128
    public string m_strNNB = "";

    // Token: 0x04000081 RID: 129
    public string m_strUserAgent = "";
}

